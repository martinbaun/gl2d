![Black Logo](https://gitlab.com/martinbaun/swat-em/-/raw/master/doc/source/fig/logo.png)

[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT) [![Actions Status](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)



gl2d
====

Graphic Library in 2D


Installation
------------

For installation execute:

```
pip install gl2d
```

or download it from [https://gitlab.com/martinbaun/gl2d/](https://gitlab.com/martinbaun/gl2d/) and
install it from the project root directory:

```
python setup.py install
```


Documentation
-------------
Find the docs on [https://swat-em.readthedocs.io/en/latest/index.html#](https://gl2d.readthedocs.io/en/latest/index.html#)
