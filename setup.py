import setuptools
import gl2d

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="gl2d",
    version=gl2d.__version__,
    author=gl2d.__author__,
    author_email="mar.baun@googlemail.com",
    description="Graphic Library in 2D",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/martinbaun/gl2d",
    packages=setuptools.find_packages(),
    #  packages=['swat_em'],
    #  package_data={'swat_em': ['themes/*', 'ui/*', 'ui/icons/*',
                              #  'ui/bitmaps/*', 'doc', 'doc/*',
                              #  'template/*']},
    platforms="any",
    install_requires=['numpy', 'matplotlib'],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    #  entry_points={"gui_scripts": [
    #  "swat-em = swat_em.main:main"]},
)
