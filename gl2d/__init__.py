# -*- coding: utf-8 -*-

import os

__title__ = "gl2d - Graphic Library in 2d"
__version__ = "0.0.1"
__author__ = "Martin Baun"
__license__ = "MIT License"
__copyright__ = "Copyright 2020-2021 Martin Baun"

#  from swat_em.datamodel import datamodel, project

from .vecgeo import Point, Line, Point_polar, Circle, Arc, radius_between_lines, Surface
from .sketch import Sketch
