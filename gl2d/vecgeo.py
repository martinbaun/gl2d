# -*- coding: utf-8 -*-
import os
import sys
import numpy as np
from collections import Counter

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), "..")))


import matplotlib.pyplot as plt
from matplotlib.patches import PathPatch
from matplotlib.path import Path
import copy

class Point:
    def __init__(self, x=0, y=0, size = 0):
        self.x = x
        self.y = y
        self.size = size

    @property
    def xy(self):
        return (self.x, self.y)
        
    def __eq__(self, other):
        return self.x==other.x and self.y==other.y
        
    def __add__(self, other):
        return Point(self.x+other.x, self.y+other.y)
        
    def __sub__(self, other):
        return Point(self.x-other.x, self.y-other.y)
    
    def __str__(self):
        return 'Point at: ' + str(self.x) + ', ' + str(self.y)
        
    def __repr__(self):
        return self.__str__()
    
    def tangent_to_circle(self, circle, side):
        '''
        returns point on circle for tangent this point and the circle
        side = 'left' or 'right'
        '''
        return tangent_to_circle(self, circle, side)
    
    @property
    def complex(self):
        return self.x + 1j*self.y
    
    @property
    def angle(self):
        return np.angle(self.complex)
    
    @property
    def len(self):
        return np.abs(self.complex)
    
    def draw(self, color='k', text = None):
        plt.plot(self.x, self.y, 'o', color=color)
        if text is not None:
            plt.text(self.x, self.y, text)

        

class Point_polar(Point):
    def __init__(self, r, alpha):
        tmp = r*np.exp(1j*alpha)
        self.x = tmp.real
        self.y = tmp.imag


class Circle:
    def __init__(self, pm, r):
        self.pm = pm
        self.r = r

    def intersect_line(self, line):
        return intersect_line_circle(line, self)
        
        
    def draw(self, color='k'):
        c = plt.Circle(self.pm.xy, self.r, color=color, fill=False)
        plt.gca().add_artist(c)
        plt.plot(self.pm.x, self.pm.y, '+', color=color)
        
        
        
class Arc:
    def __init__(self, pm, p1, p2, direction = 'left'):
        self.pm = pm
        self.p1 = p1
        self.p2 = p2
        self.direction = direction

    
    def draw(self, color='k', N = 21):
        alpha = (self.p1-self.pm).angle
        beta = (self.p2-self.pm).angle
        if self.direction == 'left':
            diff = beta - alpha
            while diff < 0:
                diff += 2*np.pi
            phi = np.linspace(alpha, alpha+diff, N)
        else:
            diff = alpha - beta
            while diff < 0:
                diff += 2*np.pi
            phi = np.linspace(alpha, alpha-diff, N)

        r = (self.p2-self.pm).len
        x = r*np.cos(phi) + self.pm.x
        y = r*np.sin(phi) + self.pm.y
        plt.plot(x, y, color = color)
        
        

class Line:
    def __init__(self, p1, p2):
        self.p1 = p1
        self.p2 = p2
    
    def __str__(self):
        return 'Line from point {} to {}'.format(self.p1.xy, self.p2.xy)
    
    @property
    def len(self):
        return np.sqrt( (self.p2.x-self.p1.x)**2 + (self.p2.y-self.p1.y)**2 )
    
    def vec(self):
        return Point(self.p2.x-self.p1.x, self.p2.y-self.p1.y)
    
    @property
    def angle(self):
        vec = self.vec()
        return np.angle(vec.x + 1j*vec.y)
    
    @property
    def xy(self):
        return ([self.p1.x, self.p1.y], [self.p2.x, self.p2.y])
        
    def param(self):
        A = 1/(self.p2.x-self.p1.x)
        B = -1/(self.p2.y-self.p1.y)
        C = self.p1.x/(self.p2.x-self.p1.x) - self.p1.y/(self.p2.y-self.p1.y)
        return (A, B, C)
    
    def intersect_line(self, line):
        return intersect_lines(self, line)
        
    def intersect_circle(self, c1):
        return intersect_line_circle(self, c1)
    
    def get_parallel(self, dist, side):
        return line_move_parallel(self, dist, side)
        
    def reverse(self):
        return Line(self.p2, self.p1)
    
    @property
    def points(self):
        return [self.p1, self.p2]
    
    def draw(self, color='k'):
        plt.plot([self.p1.x, self.p2.x], [self.p1.y, self.p2.y], color=color)


class Surface:
    def __init__(self, loop, reverse, hole=[]):
        self.loop = loop
        self.reverse = reverse
        self.hole = copy.copy(hole)

    def __eq__(self, other):
        return Counter(self.loop)==Counter(other.loop) and Counter(self.reverse)==Counter(other.reverse)

    def add_hole(self, hole):
        self.hole.append(hole)
        #  print('hole added')

    def draw_outline(self, color='k'):
        for l in self.loop:
            l.draw(color = color)
    
    def get_xy(self):
        xy = []
        for l, rev in zip(self.loop, self.reverse):
            if rev:
                xy.append(l.p2.xy)
                #  xy.append(l.p1.xy)
            else:
                xy.append(l.p1.xy)
                #  xy.append(l.p2.xy)
        xy.append(xy[0])
        
        return xy
    
    
    def contains_point(self, point):
        
        type_point = type(point)
        if type_point != type([]):
            point = [point]
        
        bbox = self.bbox()
        #  print('bbox', bbox)
        
        is_in = []
        for p in point:
            cross_line = Line(Point(bbox[0]-1, p.y), p) # vector from out of boundingbox to the point
            crossing = 0
            for line in self.loop:
                #  print('loop', line)
                #  print('cross_line', cross_line)
                if line.intersect_line(cross_line) is not None:
                    crossing += 1
            if crossing % 2 == 1: # uneven numer of crossings -> point is in polygon
                is_in.append(True)
            else:
                is_in.append(False)
            
        if type_point == type([]):
            return is_in
        else:
            return is_in[0]
    
    
    
    def bbox(self):
        x, y = [], []
        p = self.get_all_points()
        for p_ in p:
            x.append(p_.x)
            y.append(p_.y)
        xmin = np.min(x)
        xmax = np.max(x)
        ymin = np.min(y)
        ymax = np.max(y)
        return (xmin, xmax, ymin, ymax)
    
    
    
    
    def get_all_points(self):
        '''
        with last point/closed: p[0] == p[-1]
        '''
        p = []
        for l, rev in zip(self.loop, self.reverse):
            if rev:
                p.append(l.p2)
            else:
                p.append(l.p1)
        p.append(p[0])
        return p

    def draw_fill(self, color='k', alpha=0.25):
        #  xy = self.get_xy()
        #  poly = plt.Polygon(xy, color = color, alpha = alpha, linewidth=0)
        #  plt.gca().add_patch(poly)
        

        
        
        # TODO: Plot with holes!!
        xy = self.get_xy()
        
        coord = xy
        code = [Path.MOVETO] + [Path.LINETO]*(len(xy)-2) + [Path.CLOSEPOLY]
        
        for hole in self.hole:
            xy = hole.get_xy()
            coord += xy[::-1]
            code += [Path.MOVETO] + [Path.LINETO]*(len(xy)-2) + [Path.CLOSEPOLY]
        
        #  print(coord, code)
        path = Path(coord, code)
        
        patch = PathPatch(path, color = color, alpha = alpha, linewidth=0)
        plt.gca().add_patch(patch)


    @property
    def area(self):
        xy = self.get_xy()
        p = [Point(x,y) for x, y in xy]
        return abs(get_polygon_area(p))

    @property
    def area_signed(self):
        xy = self.get_xy()
        p = [Point(x,y) for x, y in xy]
        return get_polygon_area(p)

    @property
    def center(self):
        xy = self.get_xy()
        p = [Point(x,y) for x, y in xy]
        return get_polygon_center(p)


def get_polygon_area(points):
    A = 0.0
    for i in range(len(points)-1):
        A += (points[i].y + points[i+1].y) * (points[i].x - points[i+1].x)
    return 0.5*A



def get_polygon_center(points):
    points = points.copy()
    if points[-1] != points[0]:
        points.append(points[0])
    x = 0
    y = 0
    A = get_polygon_area(points)
    for i in range(len(points)-1):
        x += (points[i].x+points[i+1].x)*(points[i].x*points[i+1].y - points[i+1].x*points[i].y)
        y += (points[i].y+points[i+1].y)*(points[i].x*points[i+1].y - points[i+1].x*points[i].y)
    return Point(x/(6*A), y/(6*A))


  


def tangent_to_circle(p1, circle, side):
    '''
    tangent on circle to point p1
    returns the tangent point
    on 'left' or 'right' side
    '''
    l = Line(p1, circle.pm).len
    gamma = np.arccos(circle.r/l)
    
    if side == 'left':
        gamma = Line(p1, circle.pm).angle + np.pi - gamma
    elif side == 'right':
        gamma = Line(p1, circle.pm).angle + np.pi + gamma
    else:
        raise ValueError("side must be 'left' or 'right'")
    return circle.pm + Point_polar(circle.r, gamma)
    

def intersect_lines(l1, l2):
    
    p = intersect_lines_extended(l1, l2)
    if p is None:
        return None
    
    x1, y1 = l1.p1.xy
    x2, y2 = l1.p2.xy
    x3, y3 = l2.p1.xy
    x4, y4 = l2.p2.xy
    
    eps = 1e-14 # numeric tolerance
    crit1 = p.x >= min([x1,x2])-eps and p.x <= max([x1,x2])+eps and p.x >= min([x3,x4])-eps and p.x <= max([x3,x4])+eps
    crit2 = p.y >= min([y1,y2])-eps and p.y <= max([y1,y2])+eps and p.y >= min([y3,y4])-eps and p.y <= max([y3,y4])+eps
    
    if crit1 and crit2:
        return p
    else:
        return None
    


def intersect_lines_extended(l1, l2):
    '''intersection between two extended lines'''
    x1, y1 = l1.p1.xy
    x2, y2 = l1.p2.xy
    x3, y3 = l2.p1.xy
    x4, y4 = l2.p2.xy
    
    z = (x4-x3)*(x2*y1-x1*y2)-(x2-x1)*(x4*y3-x3*y4)
    n = (y4-y3)*(x2-x1)-(y2-y1)*(x4-x3)
    if n==0:
        return None
    x = z/n
    z = (y1-y2)*(x4*y3-x3*y4)-(y3-y4)*(x2*y1-x1*y2)
    y = z/n
    
    return Point(x, y)




#  def intersect_circle_line(circle, line):
    

def line_move_parallel(l, dist, side):
    # side = 'left' or 'right'

    alpha = l.angle
    if side == 'left':
        alpha += np.pi/2
    elif side == 'right':
        alpha -= np.pi/2
    else:
        raise ValueError("side must be 'left' or 'right'")
    p1 = l.p1 + Point_polar(dist, alpha)
    p2 = l.p2 + Point_polar(dist, alpha)
    return Line(p1, p2)


def radius_between_lines(l1, l2, r):
    
    side = 'left'
    l1b = l1.get_parallel(r, side = side)
    l2b = l2.get_parallel(r, side = side)
    pm = l1b.intersect_line(l2b)
    if pm is None:
        side = 'right'
        l1b = l1.get_parallel(r, side = side)
        l2b = l2.get_parallel(r, side = side)
        pm = l1b.intersect_line(l2b)
    if pm is None:
        raise Exception('lines not connected')
    
    alpha = l1b.angle - np.pi/2
    if side == 'right': alpha += np.pi
    p1 = pm + Point_polar(r, alpha)
    
    alpha = l2b.angle - np.pi/2
    if side == 'right': alpha += np.pi
    p2 = pm + Point_polar(r, alpha)
    return p1, p2, pm


def intersect_line_circle(l, c1):
        a, b, c = l.param()
        d = c - a*c1.pm.x - b*c1.pm.y
        x1 = c1.pm.x+(a*d + b*np.sqrt(c1.r**2*(a**2+b**2)-d**2)) / (a**2 + b**2)
        y1 = c1.pm.y+(b*d - a*np.sqrt(c1.r**2*(a**2+b**2)-d**2)) / (a**2 + b**2)
        SP1 = Point(x1, y1)

        x2 = c1.pm.x+(a*d - b*np.sqrt(c1.r**2*(a**2+b**2)-d**2)) / (a**2 + b**2)
        y2 = c1.pm.y+(b*d + a*np.sqrt(c1.r**2*(a**2+b**2)-d**2)) / (a**2 + b**2)
        SP2 = Point(x2, y2)
        return (SP1, SP2)


def get_arc_points(p1, p2, N = 21):
    a1 = np.angle(p1.x + 1j*p1.y)
    a2 = np.angle(p2.x + 1j*p2.y)
    r = np.sqrt(p1.x**2 + p1.y**2)
    phi = np.linspace(a1, a2, N)
    
    tmp = r*np.exp(1j*phi)
    #  print(tmp)
    #  print(r)
    return [point(t.real, t.imag) for t in tmp]


def rotate_point(p, dphi):
    tmp = np.array(p.x) + 1j*np.array(p.y)
    r = np.abs(tmp)
    phi = np.angle(tmp)
    
    tmp = r * np.exp(1j*(phi+dphi))
    return point(tmp.real, tmp.imag)


def rotate_points(points, dphi):
    return [rotate_point(p, dphi) for p in points]


def mirror_point_on_x(p):
    return point(p.x, -p.y)




