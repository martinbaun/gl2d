# -*- coding: utf-8 -*-
import os
import sys
import numpy as np
import matplotlib.pyplot as plt
from collections import Counter

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), "..")))
import gl2d
from gl2d import Point, Line, Point_polar, Circle, Arc, Surface
import copy
   
    
    



class Sketch:
    def __init__(self):
        self.line = []
        self.surface = []
        #  self.point = []
    
    def add_line(self, line):
        self.line.append(line)


    def add_surface(self, s):
        self.surface.append(s)


    def lines_share_point(self, p):
        '''returns all lines, that share point p'''
        l = []
        for line in self.line:
            if p in line.points:
                l.append(line)
        return l


    def get_all_points(self):
        p = []
        for l in self.line:
            p += [l.p1, l.p2]
        
        p2 = [] # remove duplicates
        for p_ in p:
            if p_ not in p2:
                p2.append(p_)
        return p2


    def get_idx_of_line(self, line):
        if type(line) == type([]):
            return [self.line.index(l) for l in line]
        else:
            return self.line.index(line)
    
    
    def plot_lines(self):
        for l in self.line:
            l.draw()

    def bbox(self):
        x, y = [], []
        p = self.get_all_points()
        for p_ in p:
            x.append(p_.x)
            y.append(p_.y)
        xmin = np.min(x)
        xmax = np.max(x)
        ymin = np.min(y)
        ymax = np.max(y)
        return (xmin, xmax, ymin, ymax)

   
    
    
    
    def find_all_closed_loop(self):

        surfaces = []
        #  print('surfaces init', surfaces)
        for line_actual in self.line:
            
            for direction in [0, 1]:
            
                polypoints = []
                polylines = []
                reverse = []
            
                polylines.append(line_actual)
                if direction == 0:
                    polypoints.append(polylines[0].p1)
                    polypoints.append(polylines[0].p2)
                    reverse.append(False)
                else:
                    polypoints.append(polylines[0].p2)
                    polypoints.append(polylines[0].p1)
                    reverse.append(True)
                
                
                i = 0
                # walk along connected lines clock-wise
                while i<10: # TODO: Use a better value
                    lines = self.lines_share_point(polypoints[-1])
                    lines = [l for l in lines if l != polylines[-1]]  # remove line, because its already detected
                    
                    lref = Line(polypoints[-1], polypoints[-2])
                    lref_angle = lref.angle
                    while lref_angle < 0:
                        lref_angle += 2*np.pi
                    
                    diff = []
                    reverse_ = []
                    for l in lines:
                        if l.p1 == polypoints[-1]:
                            l = Line(polypoints[-1], l.p2)
                            reverse_.append(False)
                        else:
                            l = Line(polypoints[-1], l.p1)
                            reverse_.append(True)

                        angle = l.angle
                        while angle < 0:
                            angle += 2*np.pi
                        #  print(l.p2.xy, 'angle:', angle)
                        d = l.angle - lref_angle
                        while d < 0:
                            d += 2*np.pi

                        diff.append(d)
                    
                    idx = np.argmin(diff)
                    polylines.append(lines[idx])
                    if lines[idx].p1 == polypoints[-1]:
                        polypoints.append(lines[idx].p2)
                    else:
                        polypoints.append(lines[idx].p1)
                    i+=1
                    reverse.append(reverse_[idx])
                    
                    if polylines[-1] == polylines[0]:
                        polylines.pop(-1)
                        reverse.pop(-1)
                        break
                    

                s_ = Surface(polylines, reverse)
                #  print('s_', s_)
                if s_.area_signed < 0:  # only clock-wise closed loops
                    if s_ not in surfaces:
                        surfaces.append(s_)
        
        # TODO: detect holes
        for i in range(len(surfaces)):
            for i2 in range(len(surfaces)):
                if i2 == i:
                    continue
                is_in = surfaces[i].contains_point(surfaces[i2].get_all_points()) # for all points
                #  print(np.all(is_in))
                #  print('surf', surfaces[0])
                if np.all(is_in):
                    surfaces[i].add_hole(copy.deepcopy(surfaces[i2]))
                    #  surfaces[i].add_hole('a')
                    #  surfaces[i].add_hole(Surface(surfaces[i2].loop, surfaces[i2].reverse)  )
                    
                    #  print('surf', surfaces[i2])
                #  print('surfaces',i,i2, surfaces[0].hole)
        
        return surfaces






