# -*- coding: utf-8 -*-

import os
import sys
import numpy as np

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), "..")))
import gl2d
from gl2d import Point, Line, Point_polar, Circle, Arc
from gl2d import Sketch


try:
    __IPYTHON__
    import matplotlib.pyplot as plt
    plt.ion()
except NameError:
    import matplotlib as mpl
    mpl.use('Agg')
    import matplotlib.pyplot as plt



CREATE_PLOTS = True

geo = Sketch()

size1 = 0.25
p = [None]*8
p[0] = Point(0, 0, size = size1)
p[1] = Point(1, 0, size = size1)
p[2] = Point(2, 0, size = size1)
p[3] = Point(2, 2, size = size1)
p[4] = Point(1, 2, size = size1)
p[5] = Point(1, 1, size = size1/4)
p[6] = Point(0, 1, size = size1)
p[7] = Point(-1, 2, size = size1)

l = [None]*10
l[0] = Line(p[0], p[1])
l[1] = Line(p[1], p[5])
l[2] = Line(p[5], p[6])
l[3] = Line(p[6], p[0])  # obacht
l[4] = Line(p[1], p[2])
l[5] = Line(p[2], p[3])
l[6] = Line(p[3], p[4])
l[7] = Line(p[4], p[5])
l[8] = Line(p[4], p[7])
l[9] = Line(p[7], p[0])

for l_ in l:
    geo.add_line(l_)


#  plt.figure(1)
#  plt.clf()
#  plt.axis('equal')
#  geo.plot_lines()

#  lines = geo.lines_share_point(p[2])
#  print([geo.get_idx_of_line(l_) for l_ in lines])



s = geo.find_all_closed_loop()

l = [s_.loop for s_ in s]

from collections import Counter
c = [Counter(l_) for l_ in l]



#  for s_ in s:
    #  s_.draw_fill('g')
    #  plt.pause(1)



"""
pointer1 = Point(0.2,0.8)
pointer1.draw(color='r')
pointer1.draw(text = 'p1')
s1 = geo.find_closed_loop(pointer1)
#  s1.draw_outline(color = 'r')
s1.draw_fill(color = 'r')
s1.center.draw(text = 'Center S1')
print('area:', s1.area)


pointer2 = Point(1.5,0.5)
pointer2.draw(color='b')
pointer2.draw(text = 'p2')
s2 = geo.find_closed_loop(pointer2)
#  s1.draw_outline(color = 'b')
s2.draw_fill(color = 'b')
s2.center.draw(text = 'Center S1')
print('area:', s2.area)


#  pointer = Point(0.5,1.5)
pointer3 = Point(-0.1,0.63)
pointer3.draw(color='g')
pointer3.draw(text = 'p3')
s3 = geo.find_closed_loop(pointer3)
#  s1.draw_outline(color = 'g')
s3.draw_fill(color = 'g')
s3.center.draw(text = 'Center S1')
print('area:', s3.area)


geo.add_surface(s1)
geo.add_surface(s2)
geo.add_surface(s3)


# meshing with gmsh
sys.path.append('/usr/local/lib/python3.8/site-packages/gmsh-4.7.1-Linux64-sdk/lib')
import gmsh

gmsh.initialize()
gmsh.model.add("t1")

pnts = geo.get_all_points()
for i, p in enumerate(pnts):
    gmsh.model.geo.addPoint(p.x, p.y, 0, p.size, i+1)

for i, l in enumerate(geo.line):
    idx1 = pnts.index(l.p1) + 1
    idx2 = pnts.index(l.p2) + 1
    gmsh.model.geo.addLine(idx1, idx2, i+1)


for i, s in enumerate(geo.surface):
    idx = geo.get_idx_of_line(s.loop)
    rev = s.reverse
    
    idx2 = []
    for k in range(len(idx)):
        if rev[k]:
            idx2.append(-(idx[k]+1))
        else:
            idx2.append(idx[k]+1)
    
    gmsh.model.geo.addCurveLoop(idx2, i+1)
    gmsh.model.geo.addPlaneSurface([i+1], i+1)

gmsh.model.geo.synchronize()

#  gmsh.model.addPhysicalGroup(1, [1, 2, 4], 5)
#  ps = gmsh.model.addPhysicalGroup(2, [1])
#  gmsh.model.setPhysicalName(2, ps, "My surface")
gmsh.model.mesh.generate(2)
gmsh.write("t1.msh")

#  if '-nopopup' not in sys.argv:
    #  gmsh.fltk.run()

nodes = gmsh.model.mesh.getNodes()
elements = gmsh.model.mesh.getElements()

gmsh.finalize()



nodes_idx = nodes[0]
nodes_xyz = nodes[1]
nodes_xyz = nodes_xyz.reshape(len(nodes_xyz)//3, 3)
nodes_x = nodes_xyz[:,0]
nodes_y = nodes_xyz[:,1]
"""


   
   




"""
sys.path.append('/usr/local/lib/python3.8/site-packages/gmsh-4.7.1-Linux64-sdk/lib')
import gmsh

# The Python API is entirely defined in the `gmsh.py' module (which contains the
# full documentation of all the functions in the API):
import gmsh
import sys


gmsh.initialize()
gmsh.model.add("t1")

lc = 1e-2
gmsh.model.geo.addPoint(0, 0, 0, lc, 1)
gmsh.model.geo.addPoint(.1, 0, 0, lc, 2)
gmsh.model.geo.addPoint(.1, .3, 0, lc, 3)
p4 = gmsh.model.geo.addPoint(0, .3, 0, lc)
gmsh.model.geo.addLine(1, 2, 1)
gmsh.model.geo.addLine(3, 2, 2)
gmsh.model.geo.addLine(3, p4, 3)
gmsh.model.geo.addLine(4, 1, p4)

gmsh.model.geo.addCurveLoop([4, 1, -2, 3], 1)
gmsh.model.geo.synchronize()

gmsh.model.addPhysicalGroup(1, [1, 2, 4], 5)
ps = gmsh.model.addPhysicalGroup(2, [1])
gmsh.model.setPhysicalName(2, ps, "My surface")
gmsh.model.mesh.generate(2)
gmsh.write("t1.msh")

# the command line arguments:
#  if '-nopopup' not in sys.argv:
    #  gmsh.fltk.run()

gmsh.finalize()
"""

