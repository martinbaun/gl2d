# -*- coding: utf-8 -*-

import os
import sys
import numpy as np

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), "..")))
import gl2d
from gl2d import Point, Line, Point_polar, Circle, Arc


try:
    __IPYTHON__
    import matplotlib.pyplot as plt
    plt.ion()
except NameError:
    import matplotlib as mpl
    mpl.use('Agg')
    import matplotlib.pyplot as plt


CREATE_PLOTS = True

    

def test_tangent_to_circle():
    p1 = Point(0, 0)
    c = Circle(Point(5,3), r=1)
    p2 = p1.tangent_to_circle(c, side='left')
    p3 = p1.tangent_to_circle(c, side='right')

    if CREATE_PLOTS:
        plt.figure(1)
        plt.clf()
        plt.title('test_tangent_to_circle')
        plt.axis('equal')
        ax = plt.gca()
        c.draw()
        p1.draw(text = 'P1')
        p2.draw(text = 'P2')
        p3.draw(text = 'P3')
        Line(p1,p2).draw()
        Line(p1,p3).draw()
        plt.savefig('test_tangent_to_circle.png')

    p2_expected = (4.3460680017760565, 3.7565533303732397)
    p3_expected = (5.35981435116512, 2.0669760813914664)

    np.testing.assert_allclose(p2_expected, p2.xy)
    np.testing.assert_allclose(p3_expected, p3.xy)


def test_intersect_lines():
    l1 = Line( Point(1,0), Point(2,2) )
    l2 = Line( Point(2,1), Point(0.5,0.5) )
    p = l1.intersect_line(l2)
    
    if CREATE_PLOTS:
        plt.figure(1)
        plt.clf()
        plt.title('test_intersect_lines')
        plt.axis('equal')
        l1.draw()
        l2.draw()
        p.draw(text='SP')
        plt.savefig('test_intersect_lines.png')

    p_expected = (1.4, 0.8)
    np.testing.assert_allclose(p_expected, p.xy)


def test_move_lines_parallel():
    l1 = Line( Point(1,0), Point(2,0.5) )
    l2 = Line( Point(2,0.5), Point(2.5,1.5) )
    l1b = l1.get_parallel(0.1, side = 'left')
    l2b = l2.get_parallel(0.1, side = 'right')
    pm = l1b.intersect_line(l2b)

    if CREATE_PLOTS:
        plt.figure(1)
        plt.clf()
        plt.title('test_move_lines_parallel')
        plt.axis('equal')
        l1.draw()
        l2.draw()
        l1b.draw(color='r')
        l2b.draw(color='r')
        plt.savefig('test_move_lines_parallel.png')

    l1b_expected = ([0.9552786404500042, 0.08944271909999159],
                    [1.9552786404500042, 0.5894427190999916])
    l2b_expected = ([2.0894427190999916, 0.4552786404500042],
                    [2.5894427190999916, 1.4552786404500042])

    np.testing.assert_allclose(l1b_expected, l1b.xy)
    np.testing.assert_allclose(l2b_expected, l2b.xy)


def test_radius_between_lines():
    l1 = Line( Point(1,0), Point(2,0.5) )
    l2 = Line( Point(2,0.5), Point(1.75,1.5) )
    l3 = Line( Point(1.75,1.5), Point(3.0,1.25) )
    p1, p2, pm = gl2d.radius_between_lines(l1, l2, 0.5)
    p3, p4, pm2 = gl2d.radius_between_lines(l2, l3, 0.2)

    if CREATE_PLOTS:
        plt.figure(1)
        plt.clf()
        plt.title('test_radius_between_lines')
        plt.axis('equal')
        l1.draw()
        l2.draw()
        l3.draw()
        p1.draw(text = 'p1')
        p2.draw(text = 'p2')
        pm.draw(text = 'pm')
        p3.draw(text = 'p3')
        p4.draw(text = 'p4')
        pm2.draw(text = 'pm2')
        Arc(pm, p1, p2).draw(color = 'r')
        Arc(pm2, p3, p4, direction = 'right').draw(color = 'r')
        plt.savefig('test_radius_between_lines.png')

    np.testing.assert_allclose((1.4176511539591605, 0.7678425713545276), pm.xy)
    np.testing.assert_allclose((1.6412579517091395, 0.3206289758545697), p1.xy)
    np.testing.assert_allclose((1.9027224040318265, 0.8891103838726941), p2.xy)

    np.testing.assert_allclose((2.0206794488598008, 1.2419033296843287), pm2.xy)
    np.testing.assert_allclose((1.8266509488307343, 1.193396204677062), p3.xy)
    np.testing.assert_allclose((2.0599026758874377, 1.4380194648225126), p4.xy)



def test_intersect_line_circle():
    l = Line( Point(-1,0), Point(2,2.5) )
    c1 = Circle(Point(1,1), r=1)
    SP1, SP2 = l.intersect_circle(c1)
    assert l.intersect_circle(c1) == c1.intersect_line(l)

    if CREATE_PLOTS:
        plt.figure(1)
        plt.clf()
        plt.title('test_intersect_line_circle')
        plt.axis('equal')
        l.draw()
        c1.draw()
        SP1.draw(text = 'SP1')
        SP2.draw(text = 'SP2')
        plt.savefig('test_intersect_line_circle.png')

    np.testing.assert_allclose((0.01230780991809477, 0.8435898415984122), SP1.xy)
    np.testing.assert_allclose((1.3319544851638727, 1.943295404303227), SP2.xy)




    
"""


    
    geo = sketch()
    
    p = [None]*8
    p[0] = point(0, 0)
    p[1] = point(1, 0)
    p[2] = point(2, 0)
    p[3] = point(2, 2)
    p[4] = point(1, 2)
    p[5] = point(1, 1)
    p[6] = point(0, 1)
    p[7] = point(-1, 2)
    
    l = [None]*10
    l[0] = line(p[0], p[1])
    l[1] = line(p[1], p[5])
    l[2] = line(p[5], p[6])
    l[3] = line(p[0], p[6])  # obacht
    l[4] = line(p[1], p[2])
    l[5] = line(p[2], p[3])
    l[6] = line(p[3], p[4])
    l[7] = line(p[4], p[5])
    l[8] = line(p[4], p[7])
    l[9] = line(p[7], p[0])
    
    for l_ in l:
        geo.add_line(l_)
    
    plt.figure(1)
    plt.clf()
    geo.plot_lines()
    
    lines = geo.lines_share_point(p[2])
    #  print([geo.get_idx_of_line(l_) for l_ in lines])
    
    pointer = point(0.5,0.5)
    pointer.plot()
    
    geo.find_closed_loop(pointer)
"""


if __name__ == '__main__':
    test_tangent_to_circle()
    test_intersect_lines()
    test_move_lines_parallel()
    test_radius_between_lines()
    test_intersect_line_circle()
    
    
